import { basePath } from "./base-path";

export default class BaseCall{
    constructor(){}
    static api(path: string, queryParams?: any){
        const url: any = new URL(basePath);
        if (path) {
            url.pathname = url.pathname + path;
        }
    
        if (queryParams) {
            for (let key in queryParams) {
            url.searchParams.append(key, queryParams[key]);
            }
        }
    
        return fetch(url);
    }
}